#include "Semester.h" 
#include <iostream> 
#include <iomanip> 
using namespace std; 
  
Semester::Semester(const string& name, const Date& start, const Date& end):Start(start), End(end) 
{ 
    this->setName(name); 
} 
  
Semester& Semester::setName(const string& name) 
{ 
    semName = name; 
    return *this; 
} 
  
Semester& Semester::setStart(int day, int month, int year) 
{ 
    Start.setDay(day).setMonth(month).setYear(year); 
    return *this; 
} 
  
Semester& Semester::setEnd(int day, int month, int year) 
{ 
    End.setDay(day).setMonth(month).setYear(year); 
    return *this; 
} 
  
ostream& operator<<(ostream& output, const Semester& Sem) 
{ 
    output << "Semester:  " << Sem.getName() << " ("
           << setfill('0') << setw(2) << Sem.getStart().getMonth() 
           << "/" << setw(2) << Sem.getStart().getDay() << "/" << Sem.getStart().getYear() 
           << "-" << Sem.getEnd().getMonth() << "/" << Sem.getEnd().getDay() << "/"
           << Sem.getEnd().getYear() << ")"; 
    return output; 
} 
  
istream& operator>>(istream& input, Semester& Sem) 
{ 
    getline(input, Sem.semName); 
    input >> Sem.Start; 
    input >> Sem.End; 
    return input; 
}