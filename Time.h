#ifndef TIME_H
#define TIME_H
#include <iostream>
#include <string>
using namespace std;
class Time
{
	friend ostream& operator<<(ostream&, const Time&);
	friend istream& operator>>(istream&, Time&);
      private:
              int hour;  //0-23 (24 hour clock format)
              int minute; //0-59
			  void standToUni(const string&);
      public:
			 Time(int = 0, int = 0);
             Time(int, int, const string&);
             
             void setTime(int, int);
             Time& setHour(int);
             Time& setMinute(int);

             int getHour() const;
             int getMinute() const;
             
             void printUniversal() const;

			 double operator-(const Time&);
};

#endif TIME_H