#include "CourseSchedule.h" //only need this since Time.h, Date.h, Course.h, Semester.h, <string>, <iomanip>, <iostream> are all included in it 
using namespace std; 
  
Course courseToEnter(); 
  
int main() 
{ 
    string studentName; 
    string semName; 
    Date startDate; 
    Date endDate; 
    int max; 
    char selection; //used for menu selection 
    cout << "Enter your name: "; 
    getline(cin, studentName); 
    cout << "Enter the semester for which you would like to create a schedule: "; 
    getline(cin, semName); 
    cout << "Enter the start date of the semester: "; 
    cin >> startDate; 
    cout << "Enter the end date of the semester: "; 
    cin >> endDate; 
    cout << "Enter the maximum amount of classes you are allowed: "; 
    cin >> max; 
    Semester Sem(semName, startDate, endDate); 
    CourseSchedule Sched(studentName, Sem, max); 
    do
    { 
        cout << "\nCOURSE ENTRY MENU FOR: " << semName << "(" << startDate << "-"
         << endDate << ")" << "\n--------------------------------------------------\n"
         << "1) Enter a new course\n" << "2) Remove a course\n" << "3) Print a Semester Schedule\n" << "q) Quit the program\n\n"
         << "Enter selection: "; 
        cin>>selection; 
        cin.ignore(); 
        string courseToRemove; 
        Course courseToAdd; 
        switch(selection) 
        { 
        case('1'): 
            courseToAdd = courseToEnter(); 
            Sched.addCourse(courseToAdd); 
            break; 
        case('2'): 
            cout << "Enter the course name of the course you wish to remove: "; 
            getline(cin, courseToRemove); 
            Sched.removeCourse(courseToRemove); 
            break; 
        case('3'): 
            cout << Sched; 
            break; 
        case('Q'): 
        case('q'): 
            break; 
        default: 
            cout << "Invalid selection\n"; 
            break; 
        } 
    }while(selection != 'Q' && selection != 'q'); 
    system("PAUSE"); 
    return 0; 
} 
  
Course courseToEnter() 
{ 
    string courseNum; 
    string courseName; 
    string meetingDays; 
    int units; 
    Time start; 
    Time end; 
    Date startdate; 
    Date enddate; 
    cout << "Enter the course number: "; 
    getline(cin, courseNum); 
    cout << "Enter the course name: "; 
    getline(cin, courseName); 
    cout << "Enter the meeting days for the course: "; 
    getline(cin, meetingDays); 
    cout << "Enter the number of units of the course: "; 
    cin >> units; 
    cout << "Enter the starting time for the class HH:MM AM (or PM): "; 
    cin >> start; 
    cout << "Enter the ending time for the class: "; 
    cin >> end; 
	cin.ignore();
    cout << "Enter the starting date for the class: "; 
    cin >> startdate; 
    cout << "Enter the ending date for the class: "; 
    cin >> enddate; 
    Course course(courseNum, courseName, meetingDays, units, startdate, enddate, start, end); 
    return course; 
}