#ifndef COURSE_H
#define COURSE_H

#include <string>

#include "Time.h"
#include "Date.h"

using namespace std;

class Course
{

private:

	string coursenumber;
	string coursename;
	string meetingdays;
	int courseunits;

	Date startdate;
	Date enddate;

	Time starttime;
	Time endtime;


public:
	//constructor and destructor
	Course(const string & = "-",const string & = "-", const string & = "", int = 0, Date & = Date(), Date & = Date(), Time & = Time(), Time & = Time());
	~Course();


	friend ostream & operator<<(ostream &, const Course &);

	// get functions
	string getCourseNumber()  const;
	string getCourseName  ()  const;
	string getMeetingdays ()  const;
	int    getCourseUnits ()  const;


	Date getStartDate()  const;
	Date getEndDate()    const;

	Time getStartTime() const;
	Time getEndTime() const;

	Course & setCourseNumber(string);
	Course & setCourseName(string);
	Course & setMeetingDays(string);
	Course & setCourseUnits(int);

	Course & setStartDate(const Date &);
	Course & setEndDate(const Date &);
	Course & setStartTime(const Time &);
	Course & setEndTime(const Time &);

	double calcDailyDuration();


};




#endif COURSE_H

