#ifndef COURSE_SCHEDULE_H
#define COURSE_SCHEDULE_H

#include "Time.h"
#include "Date.h"
#include "Course.h"
#include "Semester.h"


using namespace std;


class CourseSchedule
{

friend ostream & operator<<(ostream &, const CourseSchedule &);

private:
	string studentname;
	const Semester semester;
	Course * coursearr;
	const int MAXSIZE;
	int numcourses;

	bool checkDates( const Date &, const Date &);
	bool operator=(CourseSchedule &);
	CourseSchedule(CourseSchedule &);
	

public:
	// these two are deleted to prohibit their use. C++11 allows you to set a function that has a default value to be deleted which
	// will throw a syntax error anytime a developer tries to use them
	CourseSchedule(const string & = "", const Semester & = Semester(), int = 0);
	~CourseSchedule();

	CourseSchedule & addCourse(const Course &);

	const string & getStudentName() const;
	const Semester & getSemester() const;
	int getNumberCourses() const;


	void setStudentName(const string &);

	bool removeCourse(const string &);

};






#endif