#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

#include "Course.h"


Course::Course(const string & coursenumber, const string & coursename,const string & meetingdays, int courseunits, Date& startdate, Date& enddate, Time& starttime, Time& endtime)
{
	this->setCourseNumber(coursenumber);
	this->setCourseName(coursename);
	this->setMeetingDays(meetingdays);
	this->setCourseUnits(courseunits);

	this->setStartDate(startdate).setEndDate(enddate);
	this->setStartTime(starttime).setEndTime(endtime);

}

Course::~Course()
{
	//cout << "\n Course # " << this->getCourseNumber() << " has been deleted\n";
}


 ostream & operator<<(ostream & output, const Course & course)
 {

	 cout << "Course Info   :\t" << course.getCourseNumber() << "\t" << course.getCourseName() << endl;
	 cout << "# of Units    :\t" << course.getCourseUnits() << endl;
	 cout << "Meeting Days  :\t" << course.getStartDate() << " -- " << course.getEndDate() << endl;
	 cout << "Meeting Time  :\t" << course.getStartTime() << " -- " << course.getEndTime() << endl;
	 cout << "Class Duration:\t" << course.getStartTime()-course.getEndTime() << endl;

	 return output;
 }

	// get functions
	string Course::getCourseNumber()  const
	{
		return this->coursenumber;
	}
	string Course::getCourseName  ()  const
	{
		return this->coursename;
	}
	string Course::getMeetingdays ()  const
	{
		return this->meetingdays;
	}
	int Course::getCourseUnits ()  const
	{
		return this->courseunits;
	}


	Date Course::getStartDate()  const
	{
		return this->startdate;
	}
	Date Course::getEndDate()    const
	{
		return this->enddate;
	}

	Time Course::getStartTime() const
	{
		return this->starttime;
	}
	Time Course::getEndTime() const
	{
		return this->endtime;
	}


	Course & Course::setCourseNumber(string coursenum)
	{
		this->coursenumber = coursenum;
		return *this;
	}
	Course & Course::setCourseName(string coursename)
	{
		this->coursename = coursename;
		return *this;
	}
	Course & Course::setMeetingDays(string meetingdays)
	{
		this->meetingdays = meetingdays;
		return *this;
	}
	Course & Course::setCourseUnits(int units)
	{
		this->courseunits = units;
		return *this;
	}


	Course & Course::setStartDate(const Date & date)
	{
		this->startdate=date;
		return *this;
	}
	Course & Course::setEndDate(const Date & date)
	{
		this->enddate = date;
		return *this;
	}
	Course & Course::setStartTime(const Time & time)
	{
		this->starttime = time;
		return *this;
	}
	Course & Course::setEndTime(const Time & time)
	{
		this->endtime = time;
		return *this;
	}

	double Course::calcDailyDuration() 
	{
		return (this->starttime-this->endtime);
	}