#ifndef DATE_H
#define DATE_H

#include <iostream>
#include <stdexcept>
#include <string>
#include <iomanip>

using std::ostream;
using std::istream;
class Date
{
	// friends of the class, overloading cout and cin operators
	friend ostream & operator<<(ostream &, const Date &);
	friend istream & operator>>(istream &, Date &);

	private:
		int month; //1-12
		int day; //1-31 depending on month
		int year; //any year

		// utility functions that validate
		bool validateDay(int) const;
		bool validateMonth(int) const;
		bool validateYear(int) const;
        
	public:

        Date(int = 1, int = 1, int = 1990);

		// set functions
		Date & setMonth(int);
		Date & setDay(int);
		Date & setYear(int);

		// get functions
		int getMonth() const;
		int getDay() const;
		int getYear() const;

		int getMonthDays(int month) const;


		//overloading binary operators
		bool operator< (const Date &) const;
		bool operator<=(const Date &) const;
		bool operator> (const Date &) const;
		bool operator>=(const Date &) const;
		bool operator==(const Date &) const;
		bool operator!=(const Date &) const;
		//Date & operator=(const Date &);




};

#endif