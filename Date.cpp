#include <iostream> 
#include <string> 
  
using std::ostream; 
using std::istream; 
using namespace std; 
  
#include "Date.h" 

  
// FRIEND FUNCTIONS THAT HANDLE CIN/COUT BELOW 
  ostream & operator<<(ostream & output, const Date & date) 
 { 
     output << setfill ('0') << setw(2) << date.getMonth() << setw(1) << '/' << setw(2) << date.getDay() << setw(1) << '/' << setw(4) << date.getYear(); 
     return output; 
 } 
  
  istream & operator>>(istream & input, Date & date) 
 { 
     // this string holds the entire input (ex. 12/04/1992) from the user  
     string datehold; 
     getline(input, datehold); 
  
     // these strings will hold the relevant parts of the datehold string once it is parsed 
     string monthhold; 
     string dayhold; 
     string yearhold; 
  
     // this int will loop through the indices of datehold 
     int i = 0; 
     // this next part parses and tokenizes the input string to extract the month, day, and year.  
     for( ; i < datehold.length() && datehold[i] != '/' && datehold[i] != '\\' && datehold[i] != '-' && datehold[i] != ' '; i++) 
     { 
        monthhold.push_back(datehold[i]); 
     } 
     i++; 
     for( ;i < datehold.length() && datehold[i] != '/' && datehold[i] != '\\' && datehold[i] != '-' && datehold[i] != ' '; i++) 
     { 
         dayhold.push_back(datehold[i]);  
     } 
     i++; 
     for( ; i < datehold.length(); i++) 
     { 
         yearhold.push_back(datehold[i]);  
     } 
  
     // this next part turns the strings into ints 
     int month = atoi(monthhold.c_str()); 
     int day   = atoi(dayhold.c_str()); 
     int year  = atoi(yearhold.c_str()); 
  
     try
     { 
         date.setMonth(month).setDay(day).setYear(year); 
     } 
     catch(exception &e) 
     { 
         cout << e.what() << "\n"; 
     } 
      
     // for cascading purposes return cin 
     return input; 
 } 
  
  
  // CONSTRUCTOR BELOW 
  Date::Date(int month, int day, int year) 
  { 
      try
      { 
        this->setMonth(month).setDay(day).setYear(year); 
      } 
      catch(exception &e) 
      { 
          cout << e.what() << "\n"; 
          // since the passed in data failed, we set to default values 
          this->setMonth(1).setDay(1).setYear(1990); 
      } 
  } 

  Date & Date::setMonth(int month) 
  { 
  
      if(this->validateMonth(month)) 
          this->month = month; 
      else
          throw logic_error("invalid month entry"); 
  
      return *this; 
  } 
  Date & Date::setDay(int day) 
  { 
      if(this->validateDay(day)) 
          this->day = day; 
      else
          throw logic_error("invalid day entry"); 
  
      return *this; 
  } 
  Date & Date::setYear(int year) 
  { 
      if(this->validateYear(year)) 
          this->year = year; 
      else
          throw logic_error("invalid year entry"); 
  
  
      return *this; 
  } 
  
  int Date::getMonth() const
  { 
      return this->month; 
  } 
  int Date::getDay()  const
  { 
      return this->day; 
  } 
  int Date::getYear() const
  { 
      return this->year; 
  } 
  
  int Date::getMonthDays(int month) const 
  { 
      static int daysarray[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}; 
  
      return daysarray[month-1]; 
  } 
  
  bool Date::validateDay(int day) const 
  { 
      return (day > 0 && day <= getMonthDays(this->month)); 
  } 
  bool Date::validateMonth(int month) const 
  { 
     return  (month > 0 && month <= 12); 
  } 
  bool Date::validateYear(int year) const 
  { 
      return (year > 1900 && year < 2100); 
  } 
  
  
  // please read this next section of code carefully, it turns out you can define the <, <=, >, >=, != operator overloads by defining just 
  // < or > and the == operators. The functionality of all the other operators is easily deduced from whether two objects are equal or less than/greater than eachother.
  // I choose to define the '<' operator, it would work just the same if you defined the '>' operator
  bool Date::operator< (const Date & date2) const
  { 
      if(this->getYear() < date2.getYear()) 
	  {
          return true; 
	  }
      else if(this->getYear() > date2.getYear()) 
	  {
          return false;
	  }
      // if the above two failed, then the years are equal so we check the months 
      else
      { 
          if(this->getMonth() < date2.getMonth()) 
              return true; 
          else if(this->getMonth() > date2.getMonth()) 
              return false; 
          // if the above two failed, then the months and years are equal and we check the days 
          else
              return (this->getDay() < date2.getDay()); 
      }// end first else 
  
  }// end operator less then function 
  bool Date::operator<=(const Date & date2) const
  { 
      return !(*this > date2); // if it isn't greater than date2, it must be less than or equal to it!
  }
  bool Date::operator> (const Date & date2) const
  { 
      return(!(*this < date2) && !(*this==date2)); // if it isn't less than date2 and it's not equal to date2, it must be greater than date2!
  }
  bool Date::operator>=(const Date & date2) const
  {  
	  return !(*this < date2); // if it isn't less than date2, it must be greater than or equal to date2!
  }
  bool Date::operator==(const Date & date2) const
  { 
      return (this->getYear() == date2.getYear() & this->getMonth() == date2.getMonth() && this->getDay() == date2.getDay()); 
  } 
  bool Date::operator!=(const Date & date2) const
  { 
      return !(*this==date2); 
  }

