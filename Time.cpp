#include <iostream>
#include <iomanip>
#include <string>
#include <cmath>
using namespace std;

#include "Time.h"

ostream& operator<<(ostream& output, const Time& timeObj1) //prints in standard time
{
	output << ((timeObj1.getHour() == 0 || timeObj1.getHour() == 12) ? 12 : timeObj1.getHour() % 12) << ":" 
           << setfill ('0') << setw(2) << timeObj1.getMinute();
	if(timeObj1.getHour() >= 0 && timeObj1.getHour() < 12)
	{
		output << "AM";
	}
	else
	{
		output << "PM";
	}
		   return output;
}

istream& operator>>(istream& input, Time& timeObj2)
{
	string timeOfDay;
	input >> timeObj2.hour;
	timeObj2.hour = (timeObj2.hour >= 1 && timeObj2.hour <= 12) ? timeObj2.hour : 12;
	input.ignore();
	input >> timeObj2.minute;
	timeObj2.minute = (timeObj2.minute >= 0 && timeObj2.minute < 60) ? timeObj2.minute : 0;
	input.ignore();
	input >> timeOfDay;
	timeObj2.standToUni(timeOfDay);
	return input;
}

double Time::operator-(const Time& secondTime)
{
	return (abs(this->getHour() - secondTime.getHour()) + (abs(this->getMinute() - secondTime.getMinute())/60.0));
}

void Time::standToUni(const string& partOfDay)
{
	if(partOfDay == "PM")
	{
		if(this->getHour() < 12)
		{
			this->hour += 12;
		}
	}
	else if(partOfDay == "AM")
	{
		if(this->getHour() == 12)
		{
			this->setHour(0);
		}
	}
	else //set to midnight if an invalid string is entered
	{
		setTime(0, 0);
	}
}

Time::Time(int hour, int minute)
{
	setTime(hour, minute);
}

Time::Time(int hour, int minute, const string& amPm)
{
 this->hour = (hour >=0 && hour <= 12) ? hour : 0; 
 this->setMinute(minute);
 standToUni(amPm);
}

void Time::setTime(int hour, int minute)
{
 setHour(hour).setMinute(minute);
}

Time& Time::setHour(int h)
{
  hour = (h >= 0 && h < 24) ? h : 0;
  return *this;
}  

Time& Time::setMinute(int m)
{
  minute = (m >= 0 && m < 60) ? m : 0;
  return *this;
} 

int Time::getHour() const
{
    return hour;
}

int Time::getMinute() const
{
    return minute;
}

void Time::printUniversal()const
{
     cout << hour << ":" << setfill('0') << setw(2) << minute;
}