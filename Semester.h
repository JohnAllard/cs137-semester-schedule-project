#ifndef SEMESTER_H
#define SEMESTER_H

#include "Date.h"
#include <string>
using namespace std;

class Semester
{
	friend ostream& operator<<(ostream&, const Semester&);
	friend istream& operator>>(istream&, Semester&);
private:
	string semName;
	Date Start;
	Date End;
public:
	Semester(const string& = "Winter 1900", const Date& = Date(), const Date& = Date());
	string getName()const
	{
		return semName;
	}
	Date getStart()const
	{
		return Start;
	}
	Date getEnd()const
	{
		return End;
	}
	Semester& setName(const string&);
	Semester& setStart(int, int, int);
	Semester& setEnd(int, int, int);
};

#endif