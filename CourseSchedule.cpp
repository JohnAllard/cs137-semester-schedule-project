#include <iostream>
#include <string>
#include <iomanip>


#include "CourseSchedule.h"


using namespace std;


// get functions
	const string & CourseSchedule::getStudentName() const
	{
		return this->studentname;
	}
	const Semester & CourseSchedule::getSemester() const
	{
		return this->semester;
	}
	int CourseSchedule::getNumberCourses() const
	{
		return this->numcourses;
	}

// set functions
	void CourseSchedule::setStudentName(const string & name)
	{
		this->studentname = name;
	}

// remove a course from the course array
	bool CourseSchedule::removeCourse(const string & coursename)
	{
		int coursetoremove = -1;// error value, if it stays -1 through this loop than the user entered in a course
		for(int i = 0; i < this->numcourses; i++)	// name that doesn't exist
		{
			if(coursearr[i].getCourseName() == coursename)
				coursetoremove = i;
		}

		if(coursetoremove != -1) // this means that we found the course they want to remove
		{
			Course *newcourses = new Course[this->numcourses-1];// create a new course array of size n-1
			for(int i = 0, j = 0; i < numcourses && j < numcourses-1; i++)
			{
				if(i != coursetoremove)
				{
					newcourses[j] = coursearr[i];
					j++;
				}
			}

			this->numcourses--;
			delete [] coursearr;
			coursearr = newcourses;
			return true;
		}
		else
			return false;
	}
	CourseSchedule & CourseSchedule::addCourse(const Course & newcourse)
	{
		if( numcourses < MAXSIZE && checkDates(newcourse.getStartDate(), newcourse.getEndDate()) )
		{
			coursearr[numcourses] = newcourse;
			numcourses++;
		}
		else
		{
			throw logic_error(" Class Schedule Full ");
		}
		return *this;
	}

	bool CourseSchedule::checkDates(const Date & sdate, const Date & edate)
	{// return true if the dates of the class coincide with the dates of the semester
		return(this->semester.getStart() <= sdate && this->semester.getEnd() >= edate);
	}
	
	ostream& operator<<(ostream& output, const CourseSchedule& Sched) 
{ 
    output << "CLASS SCHEDULE" << "\n---------------\n"
           << "Name: " << Sched.getStudentName() << endl 
           << "Semester: " << Sched.semester.getName() << " (" << Sched.semester.getStart() << "-"
           << Sched.semester.getEnd() << ")\n" << "Number of Classes: " << Sched.getNumberCourses() 
           << "\n----------------------------\n"; 
    for(int i = 0; i < Sched.getNumberCourses(); i++) 
    { 
		output << "\n";
        output << Sched.coursearr[i]; 
    } 
    output << "\n\n"; 
    return output; 
} 
  
CourseSchedule::CourseSchedule(const string& name, const Semester& Sem, int max): studentname(name), semester(Sem), MAXSIZE(max) 
{ 
    this->numcourses = 0; 
    this->coursearr = new Course[this->MAXSIZE]; 
} 
  
CourseSchedule::~CourseSchedule() 
{ 
    delete [] this->coursearr; 
}